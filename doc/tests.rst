Tests
=====

Linting
-------

- `cd /path/to/Python-OC-Lettings-FR`
- `source venv/bin/activate`
- `flake8`

Tests unitaires
---------------

- `cd /path/to/Python-OC-Lettings-FR`
- `source venv/bin/activate`
- `pytest` ou `pytest --cov` pour obtenir la couverture de test