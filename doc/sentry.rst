Sentry
======

Pour utiliser sentry creez un fichier .env à la racine du projet et ajouter votre clef dns de la sorte : ::

    SENTRY_DSN = "votre_clef"

Ajouter également cette variable dans l'environnement de render pour le déploiment.