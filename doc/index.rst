.. OC LETTINGS SITE documentation master file, created by
   sphinx-quickstart on Sun Aug 20 17:22:38 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenue sur la documentation d'OC LETTINGS SITE !
===================================================

Oc lettings site est le projet réalisé avec django pour obtenir le Site web d'Orange County Lettings.

Pipeline CI/CD avec `gitlab <https://gitlab.com/istaion/OCP13>`_ et déploiement sur `Render <https://render.com/>`_.

Contents
--------

.. toctree::
   :maxdepth: 2

   installation
   tests
   sentry
   administration